package org.gdga;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(Flights.class)
public class FlightsApiTest {

	@Autowired
	MockMvc mockMvc;

	@Test
	public void should_return_static_flights() throws Exception {

		mockMvc.perform(get("/api/flights"))
				//.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().json("[{" +
						"    airline: 'THY'," +
						"    departureDate: '26/01/2018 19:30'," +
						"    departureAirport: 'IST'," +
						"    arrivalDate: '26/01/2018 18:30'," +
						"    arrivalAirport: 'AYT'," +
						"    price: '200'," +
						"    classType: 'Economy'" +
						"}]", true));
	}
}
