package org.gdga;

import java.util.Arrays;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.Data;

@SpringBootApplication
public class GdgaWorkshopApplication {

	public static void main(String[] args) {
		SpringApplication.run(GdgaWorkshopApplication.class, args);
	}
}


@RestController
class Flights {

	@GetMapping("/api/flights")
	public List<Flight> flights() {
		Flight flight = new Flight();
		flight.setAirline("THY");
		flight.setArrivalAirport("AYT");
		flight.setArrivalDate("26/01/2018 18:30");
		flight.setDepartureAirport("IST");
		flight.setDepartureDate("26/01/2018 19:30");
		flight.setPrice("200");
		flight.setClassType("Economy");

		return Arrays.asList(flight);
	}
}

@Data
class Flight {

	String airline;
	String departureDate;
	String departureAirport;
	String arrivalDate;
	String arrivalAirport;
	String price;
	String classType; // promo, economy, business
}
